def func(n)
	if(n == 0)
		return 0
	elsif(n == 1)
		return 1
	else
		return func(n - 1) + func(n - 2)
	end
end
a = 0
while(a < 500)
	x = func(a)
	print x
	print "\n"
	a = a + 1
end

