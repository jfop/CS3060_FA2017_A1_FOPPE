def func1(n)
	before = 0
	after = 1
	result = 0
	x = 0

	while(x < n)
		result = before + after
		before = after
		after = result
		x = x + 1
	end
	return result
end

a = 0
while(a < 500)
	answer = func1(a)
	print answer
	print "\n"
	a = a + 1
end