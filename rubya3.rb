def funcLowToHigh(array1)
	does_the_array_change = 1
	temp = 0
	print "\nLow to high   "
	while(does_the_array_change == 1)
		does_the_array_change = 0
		n = 0
		while(n < 99)
			if(array1[n] > array1[n + 1])
				temp = array1[n]
				array1[n] = array1[n + 1]
				array1[n + 1] = temp
				does_the_array_change = 1
			end
			n = n + 1
		end
	end

	n = 0
	while(n < 100)
		print array1[n]
		n = n + 1
	end
	return 1
end

def funcHighToLow(array1)
	does_the_array_change = 1
	temp = 0
	print "\n High to Low   "
	while(does_the_array_change == 1)
		does_the_array_change = 0
		n = 0

		while(n < 99)
			if(array1[n] < array1[n + 1])
				temp = array1[n]
				array1[n] = array1[n + 1]
				array1[n + 1] = temp
				does_the_array_change = 1
			end
			n = n + 1
		end
	end

	n = 0
	while(n < 100)
		print array1[n]
		n = n + 1
	end
	return 1
end

def funcODD(array1)
	n = 0
	print "\n Odd's   "
	while(n < 100)
		if (array1[n] % 2 == 1)
			print array1[n]
		end
		n = n + 1
	end
end

array1=Array.new(100) {rand(1...10)}
funcODD(array1)
funcLowToHigh(array1)
funcHighToLow(array1)
