class Shape
	color = "red"
	filled = true
	def Shape()
		@color = ""
		@filled = false
	end
	def Shape(color,filled)
		@color = color
		@filled = filled
	end
	def getColor()
		@color
	end
	def setColor(color)
		@color = color
	end
	def isFilled()
		@filled
	end
	def setFilled(filled)
		@filled = filled
	end
	def toString()
		"{#@color, #@filled}"
	end
end
class Circle < Shape
	radius = 1.0
	def Circle()
		@radius = 0
		@color = ""
		@filled = false
	end
	def Circle(radius)
		@radius = radius
		@color = ""
		@filled = false
	end
	def Circle(radius,color,filled)
		@radius = radius
		@color = color
		@filled = filled
	end
	def getRadius()
		@radius
	end
	def setRadius(radius)
		@radius = radius
	end
	def getArea()
		@area = 3.14 * (radius / 2)
		@area
	end
	def getPerimeter()
		@perimeter = 2 * 3.14 * radius
		@perimeter
	end
	def toString()
		"{#@radius}"
	end
end
class Rectangle < Shape
	width = 1.0
	length = 1.0
	def Rectangle()
		@width = 0
		@length = 0
		@color = ""
		@filled = false
	end
	def Rectangle(width,length)
		@width = width
		@length = length
		@color = ""
		@filled = false
	end
	def Rectangle(width,length,color,filled)
		@width = width
		@length = length
		@color = color
		@filled = filled
	end
	def getWidth()
		@width
	end
	def setWidth(width)
		@width = width
	end
	def getLength()
		@length
	end
	def setLength(length)
		@length = length
	end
	def getArea()
		@area
	end
	def getPerimeter()
		@perimeter
	end
	def toString()
		"{#@width, #@length}"
	end
end
class Square < Rectangle
	def Square()
		@color = ""
		@filled = false
		@side = 0
	end
	def Square(side)
		@side = side
		@color = ""
		@filled = false
	end
	def Square(side,color,filled)
		@side = side
		@color = color
		@filled = filled
	end
	def getSide()
		@side
	end
	def setSide(side)
		@side = side
	end
	def setWidth(side)
		@width = side
	end
	def setLength(side)
		@length = side
	end
	def toString()
		"{#@side}"
	end
end