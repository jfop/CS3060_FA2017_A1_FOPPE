def func(myfile)
	while !myfile.eof?
		line = myfile.readline
		num1 = line.split(",").first
		num2 = line.split(",").last
		num1 = num1.to_i
		num2 = num2.to_i
		result = (num1^num2).to_s(2).count("1")
		myfile.write(",#{result}")
	end
end
myfile = File.open("mytxt.txt" , "r+")
func(myfile)